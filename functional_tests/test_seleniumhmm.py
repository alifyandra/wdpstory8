from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from django.test import TestCase, Client, LiveServerTestCase
import time

class TestProject(StaticLiveServerTestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    
    def tearDown(self):
        self.browser.quit()
    
    def test_functional(self):
        self.browser.get(self.live_server_url)

        #Take content of the page
        response_content = self.browser.page_source

        self.assertIn("Alifyandra's Wonderful", response_content)

        self.browser.find_element_by_css_selector('#activities > a').click()
        time.sleep(1)

        activity_content = self.browser.find_element_by_id('activities-content').text
        self.assertIn("My current activities in this quarantine are:",activity_content)

        self.browser.find_element_by_css_selector('#commitee > a').click()
        time.sleep(1)

        commitee_content = self.browser.find_element_by_css_selector('#commitee .content > ul > li').text
        self.assertIn("Equipment staff for Fasilkom UI batch 2019 gathering",commitee_content)

        self.browser.find_element_by_css_selector('#achievements > a').click()
        time.sleep(1)

        achievements_content = self.browser.find_element_by_css_selector('#achievements .content > ul > li').text
        self.assertIn("1st Winner for Al-Azhar National English Speech",achievements_content)

        self.browser.find_element_by_css_selector('#spotify > a').click()
        time.sleep(1)

        spotify_content = self.browser.find_element_by_css_selector('#spotify .content > p > a').text
        self.assertIn("Alifyandra's Spotify",spotify_content)

        self.browser.find_element_by_css_selector('#spotify > a').click()
        time.sleep(1)
        print(self.browser.find_element_by_class_name('accordion-container').text)
        
        accordion_content = self.browser.find_element_by_class_name('accordion-container').text
        self.assertIn("Current Activities\n\nOrganizational/Comitee Experience\nAchievements\nMy Spotify Link!", accordion_content)

        self.browser.find_element_by_css_selector('#activities > .down').click()
        accordion_content_after_click = self.browser.find_element_by_class_name('accordion-container').text
        time.sleep(2)

        self.assertNotEquals(accordion_content,accordion_content_after_click)
