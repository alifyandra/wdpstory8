# Story 8
This Gitlab repository is the result of work from **Ahmad Izzudin Alifyandra**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/alifyandra/wdpstory8/badges/master/pipeline.svg)](https://gitlab.com/alifyandra/wdpstory8/commits/master)
[![coverage report](https://gitlab.com/alifyandra/wdpstory8/badges/master/coverage.svg)](https://gitlab.com/alifyandra/wdpstory8/commits/master)

## URL
This lab projects can be accessed from [https://story8accordion.herokuapp.com](https://story8accordion.herokuapp.com)

## Author
**Ahmad Izzudin Alifyandra** - [alifyandra](https://gitlab.com/alifyandra)